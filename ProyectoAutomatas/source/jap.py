﻿tokens = ('Particula1','Particula2','ParticulaAdjetivo','Objeto','SufijoPasado','SufijoPasadoNegativo','SufijoPresenteNegativo','SufijoPresente','Raiz','VerboSer', 'VerboSerNegado','VerboSerPasado','VerboSerPasadoNegativo','no')

# lexer

t_Particula1 = r'\u306f'
t_Particula2 = r'(\u3078|\u3076|\u3092|\u306b)'
t_Objeto = r'([\u4e00-\u9faf]|[\u30a0-\u30ff])+'	
t_ParticulaAdjetivo = r'\u306a|\u3044'
#(\u304D|\u3044|\u3057|\u304E|\u306B|\u3073|\u307F|\u308A|)
t_SufijoPasado = r'([\u3040-\u308f]|)\u307e\u3057\u305f'
t_SufijoPasadoNegativo = r'([\u3040-\u308f]|)\u307e\u305b\u3093\u3067\u3057\u305f'
t_SufijoPresenteNegativo = r'([\u3040-\u308f]|)\u307e\u305b\u3093'
t_SufijoPresente = r'([\u3040-\u308f]|)\u307e\u3059'
#t_Raiz = r'(\u3078|\u3076|\u3092|\u306a)'
def t_VerboSer(t):
	r'\u3067\u3059'
	t.value = "ser"
	return t
	
#t_VerboSerNegado = 
def t_VerboSerNegado(t):
	r'\u3058\u3083\u3042\u308A\u307E\u305B\u3093'
	t.value = "no ser"
	return t

#def t_VerboSerPasadoNegativo(t):
#	r'\u3058\u3083\u3042\u308A\u307E\u305B\u3093\u3067\u3057\u305F'
#	print('Encontrado2')
#	t.value = "no era"
#	return t
		
	
def t_VerboSerPasado(t):
	r'\u3067\u3057\u305f'
	#print('Encontrado1')
	t.value = "era"
	return t
	

#t_VerboSerPasadoNegativo = r'\u3067\u3083\u3042\u308a\u307e\u305b\u3093\u3067\u3057\u305f'

t_no = r'\u306e'

t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
def t_error(t):
	#lo quité porque siempre sale y no se por qué y me estresa :D
	#print("mae esa vara no existe")
	f1 = codecs.open('fails.txt', encoding='utf-8', mode='w+')
	f1.write(t.value)
	#print(t.value)
	t.lexer.skip(1)
 
import re 
import string
import ply.lex as lex
lex.lex(reflags=re.UNICODE)
from translator import translator 
t = translator()
#parser

def p_s(p):
	'S : Obj Particula1 CD'
	str = ''
	str += p[3]
	if "noser" in str:
		if p[1]=="yo":
			str = str.replace('noser','no soy')
		elif p[1]=="usted":
			str = str.replace('noser','no es')
	elif "ser" in str:
		if p[1]=="yo":
			str = str.replace('ser','soy')
		elif p[1]=="usted":
			str = str.replace('ser','es')
	else:
		if p[1]=="yo":
			words = str.split()
			if words[0]=="no":
				a=words[1]
			else:
				a=words[0]
			if a.endswith("e"):
				result = a[-1]
				a = a[:-1]
				a = a + "o"
				if words[0]=="no":
					words[1]=a
				else:
					words[0]=a
				str = ""
				for i in words:
					str += i + " "
				
	print("" + p[1] + " " + str) 
	 
def p_obj1(p):
	'Obj : Objeto no Objeto'
	s1 = t.translate(p[1])[0]
	s2 = t.translate(p[3])[0]
	if s1=="yo":
		p[0] = "mi " + s2
	elif p[1]=="usted":
		p[0] ="su " + s2
	else:
		p[0] = s1 +" de " + s2
	
def p_obj2(p):
	'Obj : Objeto ParticulaAdjetivo Objeto'
	v1 = t.translate(p[1])
	v2 = t.translate(p[3])
	p[0] = v1[0] + " " + v2[0]
def p_obj3(p):
	'Obj : Objeto'
	v = t.translate(p[1])
	p[0]=v[0]
	
def p_cd1(p):
	'CD : Obj Particula2 V1'
	#v = t.translate(p[1])
	p[0] = p[3]
	if p[2] == '\u306b':
		p[0] +=' en'
	elif p[2] == '\u3078':
		p[0] += ' a'
	
	p[0] += ' ' +p[1]
	#impresion del verbo

def p_cd2(p):
	'CD : Obj V2'
	p[0] = "" + p[2] + " " + p[1]

def p_v1(p):
	'V1 : Objeto Sufijo'
	#if len(p[2]) > 1:
	tiempo = p[2][1]
	s  = p[1]
	if p[2][0] != '\u3057':
		s  +=p[2][0]
	v = t.translate(s)
	if tiempo == 'presente':
		if v[0].endswith("er"):
			v[0]=v[0].replace("er","e")
		elif v[0].endswith("ar"):
			v[0]=v[0].replace("ar","a")
		elif v[0].endswith("ir"):
			v[0]=v[0].replace("ir","e")
	
	if tiempo == 'pasadoNegativo':
		if v[0].endswith("er"):
			v[0]="no " + v[0].replace("er","ía")
		elif v[0].endswith("ar"):
			v[0]="no " + v[0].replace("ar","aba")
		elif v[0].endswith("ir"):
			v[0]="no " + v[0].replace("ir","ía")	
			
	
	if tiempo == 'pasado':
		if v[0].endswith("er"):
			v[0]=v[0].replace("er","ía")
		elif v[0].endswith("ar"):
			v[0]=v[0].replace("ar","aba")
		elif v[0].endswith("ir"):
			v[0]=v[0].replace("ir","ía")	
			
	if tiempo == 'presenteNegativo':
		if v[0].endswith("er"):
			v[0]="no " + v[0].replace("er","e")
		elif v[0].endswith("ar"):
			v[0]="no " + v[0].replace("ar","a")
		elif v[0].endswith("ir"):
			v[0]="no " + v[0].replace("ir","e")
			
	p[0] = v[0]
def p_v3(p):
	'V1 : Sufijo'
	if p[1][0] == '\u3057':
		tiempo = p[1][1]
		
		if tiempo == "presente":
			p[0]="hace"
		elif tiempo == "presenteNegativo":
			p[0]="no hace"
		elif tiempo == "pasado":
			p[0] = "hacía"
		elif tiempo == "pasadoNegativo":
			p[0] = "no hacía"
		
	
def p_sufijo1(p):
	'Sufijo : SufijoPasado'
	l = list()
	l.append(t.getConjugation(p[1]))
	l.append('pasado')
	p[0] = l	
	
def p_sufijo2(p):
	'Sufijo : SufijoPasadoNegativo'
	l = list()
	l.append(t.getConjugation(p[1]))
	l.append('pasadoNegativo')
	p[0] = l	
def p_sufijo3(p):
	'Sufijo : SufijoPresenteNegativo'
	l = list()
	l.append(t.getConjugation(p[1]))
	l.append('presenteNegativo')
	p[0] = l	
def p_sufijo4(p):
	'Sufijo : SufijoPresente'
	l = list()
	l.append(t.getConjugation(p[1]))
	l.append('presente')
	p[0] = l	
	
def p_v21(p):
	'V2 : VerboSer'
	p[0] = p[1]

def p_v22(p):
	'V2 : VerboSerNegado'
	p[0] = p[1]

def p_v23(p):
	'V2 : VerboSerPasado'
	p[0] = p[1]

def p_v24(p):
	'V2 : VerboSerNegado VerboSerPasado'
	p[0] = "no era"

	
def p_error(p):
	print("Error en la oracion")

import ply.yacc as yacc
yacc.yacc()
import codecs
f = codecs.open('kanji.txt', encoding='utf-8', mode='r')
next = f.readline()
while next != "":
	try:
		s = next
	except EOFError:
		print("mae error")
		break
	if not s: continue
	yacc.parse(s)
	next = f.readline()
f.close()

import xml.etree.ElementTree as ET
class translator:
	def __init__(self):
		self.japaneseDictionary = ET.parse('..\\resources\\JMdict')
		self.root=self.japaneseDictionary.getroot()
		print('Objeto inicializado')
	def translate(self,kanji):
		trans = list()
		for entry in self.root.findall('entry'):
			keb = entry.find('./k_ele/keb')
			if keb != None:
				if keb.text == kanji: 
					for element in entry.findall(".//sense/gloss"):
						#print(element.get('{http://www.w3.org/XML/1998/namespace}lang') )
						if element.get('{http://www.w3.org/XML/1998/namespace}lang') == 'spa':
							trans.append(element.text)
		return trans			
	def getType(self,caracter):
		r = ''
		if '\u304D' == caracter:
			r = '\u304F'
		if '\u3044' == caracter:
			r = '\u3046'
		if '\u3057' == caracter:
			r = '\u3059'
		if '\u304E' == caracter:
			r = '\u3050'
		if '\u306B' == caracter:
			r = '\u306C'
		if '\u3073' == caracter:
			r = '\u3076'
		if '\u307F' == caracter:
			r = '\u3080'
		if '\u308A' == caracter:
			r = '\u308B'
		return r
	def getConjugation(self, str):
		s = ''
		if  str[0]== '\u3057' :
			s = '\u3057'
		else:
			if str[0] != "\u307e":
				s = self.getType(str[0])
				if s == '':
					s = str[0] + '\u308B'
			else:
				s += '\u308B'
		return s